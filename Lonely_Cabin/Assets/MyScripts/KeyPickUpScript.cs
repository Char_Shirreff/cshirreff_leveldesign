﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyPickUpScript : MonoBehaviour
{
    public Trigger_Listener_Com trigger;
    public Image cursorImage;
    public Text screenText;
    AudioSource audSource;
    MeshRenderer meshRend;
    public bool keyPickedUp = false;
    BoxCollider boxCol;

    // Use this for initialization
    void Start()
    {
        audSource = GetComponent<AudioSource>();
        meshRend = GetComponent<MeshRenderer>();
        cursorImage.enabled = false;
        boxCol = GetComponent<BoxCollider>();
    }
     
    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;
                screenText.text = "Key";
            }
        }
        else
        {
            cursorImage.enabled = false;
            screenText.text = null;
        }
    }

    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
            screenText.text = "";
        }
    }

    void OnMouseDown()
    {
        meshRend.enabled = false;
        keyPickedUp = true;
        boxCol.enabled = false;
        audSource.Play();
        Invoke("KeyAcquiredText", 1f);
        //CancelInvoke();
        Invoke("BlankText", 4f);
    }

    void KeyAcquiredText()
    {
        screenText.text = "Now to open the front door.";
    }

    void BlankText()
    {
        screenText.text = "";
    }
}

