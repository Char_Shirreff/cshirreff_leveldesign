﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour
{

    public AudioClip soundClip;

    public AudioSource soundSource;


	// Use this for initialization
	void Start ()
    {
        soundSource.clip = soundClip;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.W))
        soundSource.Play();
	}
}
