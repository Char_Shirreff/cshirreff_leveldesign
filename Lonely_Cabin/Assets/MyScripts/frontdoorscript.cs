﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class frontdoorscript : MonoBehaviour
{

    public Trigger_Listener_Com doorTrigger;
    public KeyPickUpScript key;
    public Image cursorImage;
    public Text text;

    private void OnMouseOver()
    {
        if (doorTrigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;
            }
        }
        else
        {
            cursorImage.enabled = false;
        }
    }
    private void OnMouseExit()
    {
        if (doorTrigger.playerEntered == true)
        {
            if (cursorImage.enabled == true)
            {
                cursorImage.enabled = false;
            }
        }
    }

    void OnMouseDown()
    {
        if (key.keyPickedUp == true)
        {
            text.text = "The key doesn't work. Maybe it unlocks another door?";
            Invoke("BlankText", 5f);
            
        }
        else
        {
            text.text = "It's locked.";
            Invoke("BlankText", 2f);
        }

    }

    void BlankText()
    {
        text.text = null;
    }
}

