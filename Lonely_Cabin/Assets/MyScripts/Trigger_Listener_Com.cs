﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger_Listener_Com : MonoBehaviour
{
    public bool playerEntered = false;

    // Called if player enters trigger
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            playerEntered = true;
        }
    }

    // Called if player leaves trigger
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            playerEntered = false;
        }
    }
}

