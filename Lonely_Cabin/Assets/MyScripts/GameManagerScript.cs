﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour
{
    public Text narrativeText;

	// Use this for initialization
	void Start ()
    {
        Invoke("IntroText1", 4);
	}
	
	void IntroText1()
    {
        narrativeText.text = "This blizzard's getting worse.";
        Invoke("IntroText2", 3);
    }

    void IntroText2()
    {
        narrativeText.text = "I should find a way inside that cabin.";
        Invoke("BlankText", 3);

    }

    void BlankText()
    {
        narrativeText.text = null;

    }
}
