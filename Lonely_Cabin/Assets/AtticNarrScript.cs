﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AtticNarrScript : MonoBehaviour
{
    public Trigger_Listener_Com trigger;
    public Text screenText;
    public BoxCollider boxCol;

	// Use this for initialization
	void Start ()
    {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        Invoke(screenText.text = "Ugh! It reeks, what's the owner storing up here that could smell this bad?",0.5f);
        Invoke(screenText.text = null, 4f);
    }
}
