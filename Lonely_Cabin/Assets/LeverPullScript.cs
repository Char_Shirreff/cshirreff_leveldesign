﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeverPullScript : MonoBehaviour
{
    public GameObject antler;
    public Trigger_Listener_Com trigger;
    public MouseListener_Com mouseListen;
    public float targetRot = 45;
    GameObject myPlayer;
    bool antlerPull;
    bool inTrigger;
    bool clickHappened = false;
    public float moveSpeed = 1f;
    public float snapTo = 0.1f;
    public Image cursorImage;
    public Text screenText;

    Vector3 closedRot;
    bool antlerPullable;

    // Use this for initialization
    void Start()
    {
        closedRot = transform.localRotation.eulerAngles;
    }

    void Update()
    {
        if (inTrigger)
        {
            //turn on cursor when enters switch trigger with cursor over switch  
            if (mouseListen.mouseCursorOn == true)
            {
                if (cursorImage.enabled != true)
                {
                    cursorImage.enabled = true;
                }
            }
            //turn off cursor when player leaves trigger with cursor over switch  
            else
            {
                cursorImage.enabled = false;
            }
        }

        if (inTrigger && mouseListen.mouseClicked && !clickHappened)
        {
            clickHappened = true;
            {
                LeverInteract();
            }
             Debug.Log("Open Door");
            if (antlerPullable == true)
            {
                antlerPullable = true;
            }
        }
    }

//    void OnMouseOver()
//    {
//        if (trigger.playerEntered == true)
//        {
//            if (cursorImage.enabled == false)
//            {
//                cursorImage.enabled = true;
//                screenText.text = "Pull Antler.";
//            }
//        }
//        else
//        {
//            cursorImage.enabled = false;
//            screenText.text = null;
//       }
//    }

    void LeverInteract()
    {
        Vector3 finishPos;
        if (!antlerPull)
        {

            antlerPull = true;


            finishPos = new Vector3(closedRot.x, closedRot.y, closedRot.z + targetRot);

        }
        else
        {
            finishPos = closedRot;
            antlerPull = false;
        }
        StopCoroutine("DoorMotionCo");
        StartCoroutine("DoorMotionCo", finishPos);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            inTrigger = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            cursorImage.enabled = false;
            inTrigger = false;
        }
    }

    //Moves door
    IEnumerator DoorMotionCo(Vector3 target)
    {
        Debug.Log("Door Happening");
        while (Quaternion.Angle(antler.transform.localRotation, Quaternion.Euler(target)) >= snapTo)
        {
            Debug.Log("Door Running");
            antler.transform.localRotation = Quaternion.Slerp(antler.transform.localRotation, Quaternion.Euler(target), moveSpeed * Time.deltaTime);

            yield return null;
        }
        clickHappened = false;
        antler.transform.localRotation = Quaternion.Euler(target);
        yield return null;
    }
}
