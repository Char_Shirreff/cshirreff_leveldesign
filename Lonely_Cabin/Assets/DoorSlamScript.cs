﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorSlamScript : MonoBehaviour
{

    public Trigger_Listener_Com trigger;
    public GameObject Hatch;
    public MeshRenderer meshRend;
    //public Text screenText;
    public BoxCollider boxCol;
    public Text screenText;

    // Use this for initialization
    void Start ()
    {
        meshRend.enabled = false;
        boxCol.enabled = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
         if (trigger.playerEntered == true)
         {
            meshRend.enabled = true;
            boxCol.enabled = true;
            Invoke("SlamText", 1f);
            Invoke("BlankText", 5f);
         }
        
    }

    //void OnTriggerEnter(Collider Other)
  //  {
        //if (trigger.playerEntered == true)
        // {
        //    Invoke(screenText.text = "Some snow must have fallen on the door and closed it... no going back that way now", 2f);
         //   Invoke(screenText.text = null, 5f);
        //}
  // }

    void SlamText()
    {
        screenText.text = "Some snow must have fallen on the door and closed it... no going back that way now.";
    }

    void BlankText()
    {
        screenText.text = null;
    }
}
