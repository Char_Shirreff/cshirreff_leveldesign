﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

// Quits the player when the user hits escape

public class EndGame : MonoBehaviour
{
    public Trigger_Listener_Com trigger;


    void Update()
    {
        if(trigger.playerEntered == true)
        {
            Application.Quit();
        }
    }
}
