﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackDoorKeyPickUp : MonoBehaviour
{
    public Trigger_Listener_Com trigger;
    public Image cursorImage;
    public Text screenText;
    AudioSource audSource;
    public MeshRenderer meshRend;
    public bool backKeyPickedUp = false;
    public BoxCollider boxCol;

    // Use this for initialization
    void Start()
    {
        //audSource = GetComponent<AudioSource>();
        //meshRend = GetComponent<MeshRenderer>();
        cursorImage.enabled = false;
        boxCol = GetComponent<BoxCollider>();
    }

    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;
                screenText.text = "Backdoor Key";
            }
        }
        else
        {
            cursorImage.enabled = false;
            screenText.text = null;
        }
    }

    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
            screenText.text = "";
        }
    }

    void OnMouseDown()
    {
        meshRend.enabled = false;
        backKeyPickedUp = true;
        boxCol.enabled = false;
        audSource.Play();
        Invoke("KeyAcquiredText", 0.5f);
        //CancelInvoke();
        Invoke("BlankText", 3f);
    }

    void KeyAcquiredText()
    {
        screenText.text = "RUN!";
    }

    void BlankText()
    {
        screenText.text = "";
    }
}
