﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HatchPickUp : MonoBehaviour
{
    public Trigger_Listener_Com trigger;
    public Image cursorImage;
    public Text screenText;
    MeshRenderer meshRend;
    public bool hatchPickedUp = false;
    BoxCollider boxCol;

    // Use this for initialization
    void Start()
    {
        meshRend = GetComponent<MeshRenderer>();
        cursorImage.enabled = false;
        boxCol = GetComponent<BoxCollider>();
    }

    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;
                screenText.text = "Remove Panel";
            }
        }
        else
        {
            cursorImage.enabled = false;
            screenText.text = null;
        }
    }

    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
            screenText.text = "";
        }
    }

    void OnMouseDown()
    {
        meshRend.enabled = false;
        hatchPickedUp = true;
        boxCol.enabled = false;
        //Invoke("HatchRemovedText", 0.5f);
        //CancelInvoke();
        //Invoke("BlankText", 3f);
    }

    //void HatchRemovedText()
    //{
        //screenText.text = "Panel Moved";
    //}

    //void BlankText()
    //{
        //screenText.text = "";
    //}
}
