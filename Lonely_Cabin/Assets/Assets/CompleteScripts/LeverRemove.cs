﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeverRemove: MonoBehaviour
{
    public Trigger_Listener_Com trigger;
    public Image cursorImage;
    public Text screenText;
    MeshRenderer meshRend;
    public bool antlerUsed = false;
    BoxCollider boxCol;

    // Use this for initialization
    void Start()
    {
        meshRend = GetComponent<MeshRenderer>();
        cursorImage.enabled = false;
        boxCol = GetComponent<BoxCollider>();
    }

    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;
                screenText.text = "Remove antler.";
            }
        }
        else
        {
            cursorImage.enabled = false;
            screenText.text = null;
        }
    }

    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
            screenText.text = "";
        }
    }

    void OnMouseDown()
    {
        meshRend.enabled = false;
        antlerUsed = true;
        boxCol.enabled = false;
        Invoke("AntlerRemovedText", 1f);
        //CancelInvoke();
        Invoke("BlankText", 4f);
    }

    void AntlerRemovedText()
    {
        screenText.text = "Maybe it opened something?";
    }

    void BlankText()
    {
        screenText.text = "";
    }
}