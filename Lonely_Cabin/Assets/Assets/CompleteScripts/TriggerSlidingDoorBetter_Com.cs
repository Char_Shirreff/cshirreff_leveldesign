﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSlidingDoorBetter_Com : MonoBehaviour
{

    AudioSource audioSource;

    public float doorOpenAmount = 2f;
    public Transform Door2Tran;
    public float speed = 4f;
    public float snapDistance = 0.2f;
    public BackDoorKeyPickUp key;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player" && key.backKeyPickedUp == true)
        {
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", 1f);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", 0f);
        }
    }

    IEnumerator DoorMove(float target)
    {
        audioSource.Play();
        float xPos = Door2Tran.localPosition.x;
        while (xPos < (target - snapDistance) || xPos > (target + snapDistance))
        {
            xPos = Mathf.Lerp(Door2Tran.localPosition.x, target, Time.deltaTime * speed);
            Door2Tran.localPosition = new Vector3(-xPos, 0, 0);
            Debug.Log("Moving");
            yield return null;
        }
        Door2Tran.localPosition = new Vector3(-target, 0, 0);
        Debug.Log("Stopped");
        yield return null;
    }
}
