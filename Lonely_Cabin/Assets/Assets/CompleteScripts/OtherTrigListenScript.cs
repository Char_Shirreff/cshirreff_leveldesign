﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OtherTrigListenScript : MonoBehaviour
{
    public bool playerEntered = false;
	
    //called if player enters trigger
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            playerEntered = true;
        }
    }

    //called if player leaves trigger
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            playerEntered = false;
        }
    }
}
