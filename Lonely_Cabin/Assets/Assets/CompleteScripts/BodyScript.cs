﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BodyScript : MonoBehaviour
{
    public Trigger_Listener_Com trigger;
    public Image cursorImage;
    public Text screenText;
    BoxCollider boxCol;
    public Text narrText;

    void Start()
    {
        cursorImage.enabled = false;
        boxCol = GetComponent<BoxCollider>();
    }

    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;
                screenText.text = "Eleanor's body.";
            }
        }
        else
        {
            cursorImage.enabled = false;
            screenText.text = null;
        }
    }

    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
            screenText.text = "";
        };
    }

    void OnMouseDown()
    {
            screenText.text = "That... explains the smell...";
            Invoke("LeaveText", 3f);

    }

    void LeaveText()
    {
        screenText.text = "I should leave now before the owner of the cabin comes home. Who knows what he'll do to me.";
        Invoke("BlankText", 5f);
    }

    void BlankText()
    {
        screenText.text = "";
    }
}

