﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterPickUp : MonoBehaviour
{
    public Trigger_Listener_Com trigger;
    public Image cursorImage;
    public Text screenText;
    public RawImage letterImage;
    public AudioSource audSource;
    public bool letterGot = false;
    public Text narrText;

    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled == false)
            {
                cursorImage.enabled = true;
                screenText.text = "Letter For Eleanor";
            }
        }
        else
        {
            cursorImage.enabled = false;
            screenText.text = null;
        }
    }

    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
            screenText.text = "";
        }
        letterImage.enabled = false;
        
    }

    void OnMouseDown()
    {
        if (trigger.playerEntered == true)
        {
            letterImage.enabled = true;
            audSource.Play();
            letterGot = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (letterGot == true)
        {
            Invoke("LetterText", 1f);
            //screenText.text = "Someone just parked outside... I should leave. Don't want to end up like this poor woman.";
            //CancelInvoke();
            Invoke("BlankText", 5f);
        }
       // else
        //{
            //screenText.text = "";
        //}
    }
    void LetterGot()
    {
        narrText.text = "He must be lonely... but this is wrong.";
    }

    void BlankText()
    {
        narrText.text = "";
    }
}
